#include<graphics.h>
#include<conio.h>
#include<time.h>

#define High 600	//定义画布长度与高度
#define Width 600
#define step 50	//定义方块大小

struct barcolor	//方块信息结构定义，储存各个方块的上下左右位置与颜色
{
	int top;
	int left;
	int right;
	int bottom;
	int color;
};
barcolor bars[9][9];	//定义结构二维数组

int flag = 0,p,z;	//flag用以鼠标滑动的操作，p,z,用以储存左键按下时该方块的位置
int i, j, item;	//item用以储存随机产生的颜色
int among;	//两个方块交换颜色的中间变量

void startup()
{
	srand((unsigned)time(NULL));	//设置随机种子
	initgraph(500, 500);
	setbkcolor(BLACK);
	//生成初始化方块并记录位置与颜色
	for (i = 1; i <= 8; i++)
	{
		for (j = 1; j <= 8; j++)
		{
			item = rand() % 5;
			switch (item)
			{
			case 0:setfillcolor(WHITE); break;
			case 1:setfillcolor(RED); break;
			case 2:setfillcolor(BLUE); break;
			case 3:setfillcolor(YELLOW); break;
			case 4:setfillcolor(GREEN); break;
			}
			setlinecolor(BLACK);
			fillrectangle(j * step, i * step,  (j + 1) * step,(i + 1) * step);
			bars[i][j].left = j * step;
			bars[i][j].top = i * step;
			bars[i][j].right = (j + 1) * step;
			bars[i][j].bottom = (i + 1) * step;
			bars[i][j].color = item;
		}
	}
}

void clean()
{
	
}

//显示方块
void show()
{
	for (i = 1; i <= 8; i++)
	{
		for (j = 1; j <= 8; j++)
		{
			switch (bars[i][j].color)
			{
			case 0:setfillcolor(WHITE); break;
			case 1:setfillcolor(RED); break;
			case 2:setfillcolor(BLUE); break;
			case 3:setfillcolor(YELLOW); break;
			case 4:setfillcolor(GREEN); break;
			}
			setlinecolor(BLACK);
			fillrectangle(bars[i][j].left, bars[i][j].top, bars[i][j].right, bars[i][j].bottom);
		}
	}

	FlushBatchDraw();
}

void updatewithinput()
{

	MOUSEMSG m;	//定义鼠标信息
	if (MouseHit())	//检测是否有鼠标信息的输入
	{
		m = GetMouseMsg();
		for (i = 1; i <= 8; i++)
		{
			for (j = 1; j <= 8; j++)
			{
				//左键按下选择方块
				if (m.uMsg== WM_LBUTTONDOWN &&m.x >= bars[i][j].left && m.x <= bars[i][j].right 
					&& m.y >= bars[i][j].top && m.y <= bars[i][j].bottom)
				{
					flag = 1;
					among = bars[i][j].color;	
					p = i;
					z = j;
				}
				//左键是否按下过
				if (m.uMsg == WM_LBUTTONUP &&flag==1)
				{
					//左键往左划
					if (m.x >= bars[p][z - 1].left && m.x <= bars[p][z - 1].right
						&& m.y >= bars[p][z - 1].top && m.y <= bars[p][z - 1].bottom)
					{
						bars[p][z].color = bars[p][z - 1].color;
						bars[p][z - 1].color = among;
						flag = 0;
					}
					//左键往右划
					if (m.x >= bars[p][z + 1].left && m.x <= bars[p][z + 1].right
						&& m.y >= bars[p][z + 1].top && m.y <= bars[p][z + 1].bottom)
					{
						bars[p][z].color = bars[p][z + 1].color;
						bars[p][z + 1].color = among;
						flag = 0;
					}
					//左键往下划
					if (m.x >= bars[p+1][z].left && m.x <= bars[p+1][z].right
						&& m.y >= bars[p+1][z].top && m.y <= bars[p+1][z].bottom)
					{
						bars[p][z].color = bars[p+1][z].color;
						bars[p+1][z].color = among;
						flag = 0;
					}
					//左键往上划
					if (m.x >= bars[p - 1][z].left && m.x <= bars[p - 1][z].right
						&& m.y >= bars[p - 1][z].top && m.y <= bars[p - 1][z].bottom)
					{
						bars[p][z].color = bars[p - 1][z].color;
						bars[p - 1][z].color = among;
						flag = 0;
					}
				}
			}
		}
	}

}

void updatewithoutinput()
{

}

void gameover()
{
	EndBatchDraw();
	closegraph();
}

int main()
{
	startup();
	while (1)
	{
		clean();
		updatewithoutinput();
		updatewithinput();
		show();
	}
	gameover();
	return 0;
}