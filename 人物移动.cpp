#include <graphics.h>
#include <conio.h>
int main()
{
	initgraph(610, 324);
	IMAGE img_human1,img_human2, img_background;	// 定义 IMAGE 对象
	loadimage(&img_human1, _T("E:\\行走素材图1.jpg"));
	loadimage(&img_human2, _T("E:\\行走素材图.jpg"));
	loadimage(&img_background, _T("E:\\游戏背景图.jpg"));
	int x, y; // 小人整体的坐标位置
	x = 250;
	y = 80;
	int left_i = 0;  // 向左行走动画的序号
	int right_i = 0; // 向右行走动画的序号
	char input;


	putimage(0, 0, &img_background);

	putimage(x, y, 75, 130, &img_human1, 0, 0, NOTSRCERASE);
	putimage(x, y, 75, 130, &img_human2, 0, 0, SRCINVERT);

	BeginBatchDraw();

	while (1)
	{
		if (_kbhit())  // 判断是否有输入
		{
			input = _getch();  // 根据用户的不同输入来移动，不必输入回车
			if (input == 'a') // 左移
			{

				left_i++;
				if (x > 10) // 没有达到左边的边界，才移动小人的坐标
					x = x - 10;
				putimage(0, 0, &img_background);
				putimage(x, y, 75, 130, &img_human1, left_i * 75, 0, NOTSRCERASE);
				putimage(x, y, 75, 130, &img_human2, left_i * 75, 0, SRCINVERT);
				FlushBatchDraw();
				Sleep(1);
				if (left_i == 3)
					left_i = 0;
			}
			else if (input == 'd')  // 右移
			{
	
				right_i++;
				if (x < 600) // 没有达到右边的边界，才移动小人的坐标
					x = x + 10;
				putimage(0, 0, &img_background);
				putimage(x, y, 75, 130, &img_human1, right_i * 75, 120, NOTSRCERASE);
				putimage(x, y, 75, 130, &img_human2, right_i * 75, 120, SRCINVERT);
				FlushBatchDraw();
				Sleep(1);
				if (right_i == 3)
					right_i = 0;
			}
		}
	}
	return 0;
}