#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

int fenj_x,fenj_y;
int bui_x,bui_y;
int enemy_x,enemy_y;
int high,width;
int score;
int time;
int flag;

void gotoxy(int x,int y)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X=x;
	pos.Y=y;
	SetConsoleCursorPosition(handle,pos);
}

void startup()
{
	high=20;
	width=30;
	fenj_x=high/2;
	fenj_y=width/2;
	bui_x=-1;
	bui_y=fenj_y;
	enemy_x=0;
	enemy_y=fenj_y;
	score=0;
	time=0;
	flag=0;
}

void show()
{
	gotoxy(0,0);
	int i,j;
	for(i=0;i<high;i++)
	{
		for(j=0;j<width;j++)
		{
			if((i==fenj_x)&&(j==fenj_y)||(i==fenj_x+1)&&(j>=fenj_y-2)&&(j<=fenj_y+2)||(i==fenj_x+2)&&(j==fenj_y-1)||(i==fenj_x+2)&&(j==fenj_y+1))
				printf("*");
			else if((i==enemy_x)&&(j==enemy_y))
				printf("@");
			else if((i==bui_x)&&(j==bui_y))
				printf("|");
			else
				printf(" ");			
		}
		printf("|");
		printf("\n");
	}
	for(j=0;j<width/2;j++)
		printf("��");
	printf("\n");
	printf("�÷֣� %d\n",score);
}

void updatewithoutinput()
{
	if(bui_x>-1)
		bui_x--;
		
	if((bui_x==enemy_x)&&(bui_y==enemy_y))
	{
		score++;
		enemy_x=-1;
		enemy_y=rand()%width;
		bui_x=-2;
	}
	if(enemy_x>high)
	{
		enemy_x = -1;
		enemy_y = rand()%width;
	}
	if((enemy_x==fenj_x)&&(enemy_y==fenj_y)||(enemy_x==fenj_x+1)&&(enemy_y>=fenj_y-2)&&(enemy_y<=fenj_y+2))
	{
		printf("��Ϸʧ�ܣ�\n");
		system("pause");
		exit(0);
	}
		
	static int speed=0;
	if(speed<10)
		speed++;
	if(speed==10)
	{
		enemy_x++;
		if(score%10!=0)
			flag=1;
		if(score%10==0&&time<=4&&score!=0&&flag==1)	
		{
			time=time+2;
			flag=0;
		}				
		speed=time;
	}
}

void updatewithinput()
{
	char input;
	if(kbhit())
	{
		input=getch();
		if(input=='a')
			if(fenj_y>0)
				fenj_y--;
		if(input=='d')
			if(fenj_y<width-1)
				fenj_y++;
		if(input=='w')
			if(fenj_x-2>0)
				fenj_x--;
		if(input=='s')
			if(fenj_x+2<high-1)
				fenj_x++;
		if(input==' ')
		{
			bui_x=fenj_x-1;
			bui_y=fenj_y;
		}
		if(input==27)
			system ("pause");
	}
}

int main()
{
	startup();
	while(1)
	{
		show();
		updatewithoutinput();
		updatewithinput();
	}
	return 0;
}

