#include <graphics.h>
#include <conio.h>
#include<time.h>
#include<stdio.h>

// 引用 Windows Multimedia API
#pragma comment(lib,"Winmm.lib")

IMAGE img_bk, img_bar_up1, img_bar_up2, img_bar_down1, img_bar_down2;	//储存背景与上下柱子
IMAGE img_xsbd1, img_xsbd2, img_xxbd1, img_xxbd2;	//储存向下向上飞行的小鸟
int bird_y,bird_x;	//小鸟的坐标
int bar_up_x, bar_up_y, bar_down_x, bar_down_y;	//上下挡板坐标
int score;//玩家分数
int bird_v;//小鸟下落速度
int bar_v;	//挡板的速度
int flag = 0;	//辅助挡板的速度正常增加

void startup()
{
	srand((unsigned)(time(NULL)));
	initgraph(350, 600);
	loadimage(&img_bk, _T("E:\\background.jpg"));	
	loadimage(&img_xsbd1, _T("E:\\bird_1.jpg"));
	loadimage(&img_xsbd2, _T("E:\\bird_2.jpg"));
	loadimage(&img_xxbd1, _T("E:\\bird__1.jpg"));
	loadimage(&img_xxbd2, _T("E:\\bird__2.jpg"));
	loadimage(&img_bar_up1,_T( "E:\\bar_up1.gif"));
	loadimage(&img_bar_up2,_T( "E:\\bar_up2.gif"));
	loadimage(&img_bar_down1,_T( "E:\\bar_down1.gif"));
	loadimage(&img_bar_down2, _T("E:\\bar_down2.gif"));
	bird_x = 50;	//小鸟位置初始化
	bird_y = 200;
	bar_down_x = 350;
	bar_down_y = rand() % 300 + 150;;
	bar_up_x = 350;
	bar_up_y = bar_down_y - 725;
	score = 0;	//分数初始化
	bird_v = 0;	//小鸟下落速度初始化
	bar_v = 3;	//挡板速度初始化

	BeginBatchDraw();
	
	mciSendString(_T("open E:\\background.mp3 alias bkmusic"), NULL, 0, NULL);//打开背景音乐
	mciSendString(_T("play bkmusic repeat"), NULL, 0, NULL);  // 循环播放
}

void show()
{
	putimage(0, 0, &img_bk);	// 显示背景	
	putimage(bar_up_x, bar_up_y, &img_bar_up1,NOTSRCERASE); // 显示上一半的障碍物
	putimage(bar_up_x, bar_up_y, &img_bar_up2,SRCINVERT);
	putimage(bar_down_x, bar_down_y, &img_bar_down1,NOTSRCERASE); // 显示下一半的障碍物
	putimage(bar_down_x, bar_down_y, &img_bar_down2,SRCINVERT);
	if (bird_v == 0)
	{
		putimage(bird_x, bird_y, &img_xsbd1, NOTSRCERASE); // 显示小鸟	
		putimage(bird_x, bird_y, &img_xsbd2, SRCINVERT);
	}
	else
	{
		putimage(bird_x, bird_y, &img_xxbd1, NOTSRCERASE); // 显示小鸟	
		putimage(bird_x, bird_y, &img_xxbd2, SRCINVERT);
	}
	TCHAR op[1000];
	_stprintf_s(op, _T("目前得分：%d"), score);	//显示得分
	setcolor(GREEN);
	setbkmode(TRANSPARENT);
	outtextxy(0, 0, op);
	FlushBatchDraw();
	Sleep(50);
}

void updateWithoutInput()
{
	//挡板速度随分数加快
	if (score % 5 == 0&&score>0&&flag==1)
	{
		bar_v=bar_v+2;
		flag = 0;
	}

	//小鸟下落
	if (bird_y < 500)	
	{
		bird_y = bird_y + bird_v;
		bird_v = bird_v + 2;
	}

	//下挡板后移
	if (bar_up_x > -150)
		bar_down_x = bar_down_x - bar_v;
	else
	{
		bar_down_x = 350;
		bar_down_y = rand() % 300 + 150;
		bar_up_y = bar_down_y - 725;
		score++;
		flag = 1;
	}

	//上挡板后移
	if (bar_up_x > -150)	
		bar_up_x = bar_up_x - bar_v;
	else
	{
		bar_up_x = 350;
	}		

	//判断小鸟是否与挡板碰撞
	if ((bird_x >= bar_up_x && bird_x <= bar_up_x + 150 && bird_y <= bar_up_y+600)	
		||( bird_x >= bar_up_x && bird_x <= bar_up_x + 150 && bird_y >= bar_down_y))
	{
		system("pause");

	}
}

void updateWithInput()
{
	char input;
	if(_kbhit())  // 判断是否有输入
	{
		input = _getch();  
		if (input == ' ' && bird_y>20)  
		{
			bird_y = bird_y - 45;
			mciSendString(_T("close jpmusic"), NULL, 0, NULL); // 先把前面一次的音乐关闭
			mciSendString(_T("open E:\\Jump.mp3 alias jpmusic"), NULL, 0, NULL); // 打开跳动音乐
            mciSendString(_T("play jpmusic"), NULL, 0, NULL); // 仅播放一次
			bird_v = 0;	//小鸟速度为0
		}
	}
}

void gameover()
{
	EndBatchDraw();
	_getch();
	closegraph();
}

int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		show();  // 显示画面
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}