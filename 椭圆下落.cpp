#include <graphics.h>
#include <conio.h>
#include<time.h>
#define High 480
#define Width 640
#define moneynum 10

int main()
{
	srand((unsigned)(time(NULL)));
	int money_x[moneynum], money_y[moneynum];
	int left[moneynum], right[moneynum], top[moneynum], bottom[moneynum],i;
	int money_vy;

	initgraph(Width, High);
	for (i = 0; i < moneynum; i++)
	{
		money_x[i] = rand()%Width;
		money_y[i] = rand()%10+5;
		left[i] = money_x[i] - 5;
		top[i] = money_y[i] - 10;
		right[i] = money_x[i] + 5;
		bottom[i] = money_y[i] + 10;
	}
	
	money_vy = 1;


	while (1)
	{
		setcolor(BLACK);
		setfillcolor(BLACK);
		for (i = 0; i < moneynum; i++)
			fillellipse(left[i], top[i], right[i], bottom[i]);

		for (i = 0; i < moneynum; i++)
		{
			top[i] = money_y[i] - 10;
			bottom[i] = money_y[i] + 10;
		}	

		for (i = 0; i < moneynum; i++)
			money_y[i] = money_y[i] + money_vy;

		setcolor(GREEN);
		setfillcolor(YELLOW);
		for (i = 0; i < moneynum; i++)
			fillellipse(left[i], top[i], right[i], bottom[i]);

		Sleep(10);
	}
	closegraph();
	return 0;
}